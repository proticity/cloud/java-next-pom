# Java-Next POM
This project is a parent POM to be used for modern Java development. It includes a default set of dependency-managed
library versions for common modern Java libraries such as Apache Commons, Spring, and Reactor, and common test libraries
like JUnit 5, TestNG, and Mockito. It fixes all common Maven plugins to specific modern versions.

Emphasis is placed on next-generation reactive, non-blocking Java applications and support for cloud deployments.

## Features
* Dependency management for latest stable versions of common libraries and for common public cloud environments.
* Reports:
  * Code analysis (PMD/CPD, SpotBugs, JDepend)
  * JoCoCo code coverage
  * Test results (unit and integration tests)
  * Source XRef
  * JavaDocs
  * CheckStyle report (optional)
* Optional code quality enforcement:
  * Code coverage requirements
  * SpotBugs requirements
  * CheckStyle enforcement
* Attaches source and Javadoc JARs (with preconfigured links).
* Fixed, up-to-date stable Maven plugin versions.
  * Release lifecycle plugins (maven-deploy-plugin, maven-release-plugin, maven-gpg-plugin, and
    nexus-staging-maven-plugin)
* Utility plugins ready to go with fixed versions:
  * Dependency plugin, for dependency analysis and unpacking.
  * Assembly plugin, for packaging archives.
  * Versions plugin, for managing dependency versioning.
  
## Requirements
The current goal is to support Java-Next POM for JDKs starting with the oldest LTS release of the set of newest LTS
releases supported on each major cloud service (AWS, Microsoft Azure, and Google Cloud), as well as all LTS releases
after that one, and the latest JDK (if the latest JDK is not an LTS release). The POM is tested using builds of a test
project to support the following releases. 

| JVM      | Source            | JDK Version | Suported Build Releases    | Supported Target Releases |
| -------- | ----------------- | ----------- | -------------------------- | ------------------------- |
| Corretto | Amazon            | 8           | Any                        | Any                       |
| Corretto | Amazon            | 11          | **11.0.3.7.1**<sup>†</sup> | Any                       |
| OpenJ9   | AdoptOpenJDK      | 8           | Any                        | Any                       |
| OpenJ9   | AdoptOpenJDK      | 11          | **11.0.3**<sup>†</sup>     | Any                       |
| OpenJ9   | AdoptOpenJDK      | 12          | **None**<sup>*</sup>       | Any                       |
| OpenJDK  | AdoptOpenJDK      | 8           | Any                        | Any                       |
| OpenJDK  | AdoptOpenJDK      | 11          | **11.0.3**<sup>†</sup>     | Any                       |
| OpenJDK  | AdoptOpenJDK      | 12          | **None**<sup>*</sup>       | Any                       |
| OpenJDK  | Oracle (Java.net) | 8           | Any                        | Any                       |
| OpenJDK  | Oracle (Java.net) | 11          | **11.0.3**<sup>†</sup>     | Any                       |
| OpenJDK  | Oracle (Java.net) | 12          | **12.0.1**<sup>*</sup>     | Any                       |
| OpenJDK  | Oracle (Java.net) | 13          | **None**                   | **None**                  |
| Zulu     | Azul              | 8           | Any                        | Any                       |
| Zulu     | Azul              | 11          | **11.31.11**<sup>†</sup>   | Any                       |
| Zulu     | Azul              | 12          | **12.2.3**<sup>*</sup>     | Any                       |

<small><sup><sup>†</sup> `mvn site` will fail on Java 11.0.1 through 11.0.2 due to a bug which prevents JavaDoc from
running on projects with a `module-info.java` file. It will fail on all releases of Java 11 when a dependency is a
multi-release JAR if a JavaDoc is linked for the JAR which excludes the module definition.<br/>
<sup>*</sup> `mvn site` will fail on Java 12 GA releases due to a JavaDoc bug. To build on older Java 12 releases
disable JavaDocs. JavaDoc generation will fail on all JDK 12 releases if you have a multi-release JAR dependency and
add a JavaDoc link for the library which excludes the module definition.</small>

The recommended build toolchain is currently to build with OpenJDK 12.0.1 built from Oracle (the Java.net release), and
to target older JDK versions for 8 or 11. Java 11 and Java 12 GA have serious JavaDoc bugs and therefore not considered
fully supported, although they are tested and work with JavaDoc generation disabled.

## Using the Java-Next POM
The Java-Next POM can be referenced as a parent for a project to get its default behavior for your project.

```xml
<project>
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.proticity</groupId>
        <artifactId>java-next-pom</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </parent>
    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        
        <!-- Or, if you prefer JDK 8...
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        -->
    </properties>
</project>
```

It is important to set the JDK source and target version via your `properties`. The Java-Next POM deliberately leaves
the JDK version unspecified to stay as flexible as possible for inheriting projects. It is currently built to support
JDK 8 and newer.

### Snapshot Releases
To use the snapshot releases of the POM you will need to add the snapshot repository.

```xml
<project>
    <repositories>
        <repository>
            <id>oss-snapshots</id>
            <name>Sonatype OSS Snapshots</name>
            <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
</project>
```

## Customizing the Features
### Enforcing Code Quality
By default Java-Next POM will perform reporting on code quality but does not enforce it. You can add the analysis
plugins to your `build` section to enforce minimum requirements for test code coverage, fail builds that SpotBugs finds
problems in, or fail builds that fail PMD/CPD analysis.

```xml
<project>
    ...
    <build>
        <plugins>
            <!-- Require no PMD issues. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            
            <!-- Require no SpotBugs issues. -->
            <plugin>
                <groupId>com.github.spotbugs</groupId>
                <artifactId>spotbugs-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            
            <!-- Require 70% line coverage in each class. -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <configuration>
                    <rules>
                        <rule>
                            <element>CLASS</element>
                            <limits>
                                <limit>
                                    <counter>LINE</counter>
                                    <value>COVEREDRATIO</value>
                                    <minimum>70%</minimum>
                                </limit>
                            </limits>
                        </rule>
                    </rules>
                </configuration>
            </plugin>
        </plugins>
    </build>
    ...
</project>
```

### CheckStyle Reports and Enforcement
The CheckStyle plugin is preconfigured for use with an up-to-date version of CheckStyle which supports modern (Java 11)
syntax and JDK9 modules. It defaults to the Sun coding conventions and can be overridden in your project to use your
organization's standard. Adding a reference to the plugin in your `build` section will enforce a requirement that your
code meet your CheckStyle guidelines. Adding a reference in `reporting` will generate a report as part of your Maven
site.

```xml
<project>
    ...
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <configuration>
                        ...
                    </configuration>                
                </plugin>
            </plugins>
        </pluginManagement>
        ...
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    ...
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
            </plugin>
        </plugins>
    </reporting>
    ...
</project>
```

### Documentation
JavaDocs are generated for your main sources by default and attached to the output. The attached JavaDocs JAR contains
all documented elements, including private and package-private ones. The site-generated docs include only public and
protected elements.

Links are configured by default for all dependency-managed libraries included in Java-Next POM. It is intended that by
default links will also be detected for dependencies, however this behavior is currently disabled to avoid bugs in
JavaDoc which remain present in Java 11 (as of 11.0.3) and 12 (as of 12.0.1) which can cause failures on multi-release
jars.

```xml
<build>
    <pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadocs-plugin</artifactId>
                <configuration>
                    <detectLinks>true</detectLinks>
                    <detectOfflineLinks>true</detectOfflineLinks>
                </configuration>
            </plugin>
        </plugins>
    </pluginManagement>
</build>
```

### Release Management
Java-Next is preconfigured for release management with managed versions of the `maven-gpg-plugin`,
`maven-release-plugin`, `maven-deploy-plugin`, and the `nexus-staging-maven-plugin` (for OSSRH releases or internal
Sonatype Nexus deployments).

To support deployment GPG signing is generally required. This can be easily added with the GPG plugin:

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-gpg-plugin</artifactId>
</plugin>
```

The plugin is preconfigured to support GPG2. The GPG keyring passphrase and a specific keyname, if needed, can be
specified in the `settings.xml` file (recommended) or in the configuration of the plugin.

```xml
<settings>
    <profiles>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <gpg.keyname>me@myorg.com</gpg.keyname>
                <gpg.passphrase>my_passphrase</gpg.passphrase>
            </properties>
        </profile>
    </profiles>
</settings>
```

Deployment is supported out of the box using the fixed version of the `maven-deploy-plugin` as per normal. Alternatively
use the `nexus-staging-maven-plugin`.

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
        <configuration>
            <skip>true</skip>
        </configuration>
    </plugin>
    <plugin>
        <groupId>org.sonatype.plugins</groupId>
        <artifactId>nexus-staging-maven-plugin</artifactId>
        <configuration>
            <!-- Settings for OSSRH, assuming you have an "ossrh" repo in <distributionManagement>. -->
            <serverId>ossrh</serverId>
            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
            
            <!-- Set to true to make release builds deployed be approved through staging automatically. -->
            <autoReleaseAfterClose>false</autoReleaseAfterClose>
        </configuration>
    </plugin>
</plugins>
```

The `maven-release-plugin` is already configured for projects using the Java-Next POM. Simply run `mvn release:perform`
(or any other goal of `maven-release-plugin`). By default the plugin is configured to version all submodules as well
(a good setup for a modern multi-repo setup where all projects in a single repo are closely associated). It will also,
by default, activate any profile named `release` during the release builds.

### Option Profiles
The Java-Next POM includes a number of useful profiles which can be enabled to alter behavior or change the versions of
managed dependencies. Some of these can also be enabled with environment variables.

| Profile Name  | Environment Variable   | Variable Value | Effect                                                    |
| ------------- | ---------------------- | -------------- | --------------------------------------------------------- |
| snapshots     |                        |                | Allow snapshot repositories and dependencies only available as snapshots. |
| aws-mks-1     | AWS_MKS                | 1              | Configure dependencies for Kafka 1.x in AWS MKS.          |
| aws-mks-2     | AWS_MKS                | 2              | Configure dependencies for Kafka 2.x in AWS MKS.          |
| azure-kafka-1 | AZURE_EVENT_HUBS_KAFKA | 1              | Configure dependencies for Kafka 1.x in Azure Event Hubs. |
| oracle        | ORACLE_PROPRIETARY     |                | Enable proprietary Oracle repo and dependency management. |

## Managed Dependencies
* Frameworks:
  * Spring Boot 2.x
* Networking:
  * Netty
  * Embedded Jetty
  * Embedded Tomcat
  * Servlet API 4.x
* Utility libraries:
  * Apache Commons IO, Collections 4, Lang 3, Codec, Compression, Math
  * Google Guice
  * Google Guava
  * Spring Framework
* Public cloud SDKs:
  * AWS SDKs 1.x and 2.x
  * Microsoft Azure
  * Google Cloud
  * Spring Cloud AWS, Spring Cloud Azure, and Spring Cloud GCP for Spring Boot integration
  * Spring Cloud Function for cloud-agnostic function-as-a-service
* Function reactive streams:
  * Reactor
  * Reactor Netty
  * RxJava/RxKotlin 2
* Persistence
  * JDBC drivers for PostgreSQL, MySQL, MariaDB, SQL Server, Oracle DB, H2, and HSQLDB, as well as Spring Data JDBC
  * R2DBC for reactive database connectivity, with drivers for PostgreSQL, SQL Server, and H2, as well as Spring Data
    R2DBC
  * Redis support via the non-blocking Jedis and Lettuce drivers, and Spring Data Redis
  * Cloud provider storage via DynamoDB, Neptune, Cosmos DB, and HBase
  * JPA providers Hibernate, Hibernate OGM, and EclipseLink, as well as Spring Data JPA
  * Hibernate Validators
  * ElasticSearch and Spring Data ElasticSearch
  * MongoDB and Spring Data MongoDB
  * Cassandra, with Spring Data Cassandra and Hibernate OGM support
  * Neo4j, including JDBC, Hibernate OGM, and Spring Data Neo4j support
  * Spring Data with support for the above backends
* Coordination and integration libraries
  * Apache Kafka (presets for latest and managed cloud provider versions)
  * Cloud messaging and data streams with AWS SQS/SNS, AWS Kinesis, Azure Event Hubs, and Google Pub/Sub
  * Apache Zookeeper
  * AMQP
  * ZeroMQ
  * JMX
  * Spring Messaging
  * Spring Cloud Streams and Spring Cloud Hub
* Serialization
  * Jackson
  * JSON
  * JAXB with backends from Glassfish (reference implementation), Moxy, and Xerces
* Testing
  * Junit 5 and TestNG for unit and integration testing
  * Mocking with Mockito, JMockit, and EasyMock
  * JSR305
* Debugging and Monitoring
  * SLF4J, with Logback and Log4j 2.x backends
  * Log appenders for AWS CloudWatch, Azure Application Insights, and Google Stackdriver
  * Micrometer metrics facade, with Influx, Graphite, CloudWatch, Application Insights, Stackdriver, Datadog, and
    SignalFX backends
  * OpenTracing tracing facade, including CloudWatch backend support
  * Spring Cloud Sleuth